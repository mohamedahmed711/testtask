<?php 

    function ClassLoader ($className )
    {
            $file = __DIR__ . '/' . $className . '.php';
            $ConfigFile = __DIR__ . '\config' . '/' . $className . '.php';

            if (file_exists($file)) {
                require_once $file ;
            } elseif (file_exists($ConfigFile)) {
                require_once $ConfigFile ;
            }       
    }

   spl_autoload_register('ClassLoader');