<?php
class  Controller
{
    private $DB;
    private $query = '';
    private $TableName;
    private $stmt;
    private $data = array();

    public function __construct($TableName)
    {
        $this->DB = (new Database)->DB_Connect();
        $this->TableName = $TableName;
    }
    

    public function getAll($field, $className)
    { 
        return $this->select(['*'])->where($field, '=', $className)->bind()->fetchAll(PDO::FETCH_CLASS,$className);
    }


    public function findAll(string $field, $value)
    {
        return $this->select(['*'])->where($field, '=', $value)->bind()->fetchAll(PDO::FETCH_COLUMN);   
    }

        
    public function select(array $fields)
    {   
        $this->query = 'SELECT '.implode(',', $fields).' FROM '.$this->TableName;

        return $this;
    }

    public function insert(array $data)
    {
        $this->data = array_merge($this->data, $data);

        $this->query = 'INSERT INTO '.$this->TableName.' VALUES ('.implode(',', array_fill(0, count($data), '?')).')';

        return $this->bind();
    }

    public function delete(string $field, string $value)
    {
        $this->query = 'DELETE FROM '.$this->TableName;
        $this->where($field, '=', $value);

        return $this->bind();
    }

    public function where(string $field, string $operator, string $value)
    {
        array_push($this->data, $value);
        $this->query .= ' WHERE '.$field.' '.$operator.' ?';
        return $this;
    }

    private function bind()
    {
        $this->stmt = $this->DB->prepare($this->query);
        
        if($this->data != null)
        {
            $params = array();
            $params = array_merge($params, $this->data);
            for ($i = 1; $i<=count($params) ; $i++) {
                call_user_func_array(array($this->stmt, 'bindParam'), array($i,&$params[$i-1])); 
            }
              
        }
        $this->data = array(); 
        $this->stmt->execute();
        return $this->stmt;
    }
        
}
