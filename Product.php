<?php 
abstract class Product
    {
        protected $SKU;
        protected $Name;
        protected $Price;
        protected $Type;
        protected $Properties;
        private $TableName = 'products';

        public function __construct()
        {
            
        }

        abstract public function setProperties($inputs);

        public function setinputs($inputs)
        {
        $this->setSKU($inputs['SKU']);
        $this->setName($inputs['name']);
        $this->setPrice($inputs['price']);
        $this->setType($inputs['type']);
        $this->setProperties($inputs);      
        }

        public function save()
        { 
            (new Controller($this->TableName))->insert(array($this->SKU, $this->Name,$this->Price, $this->Type, $this->Properties));
        }

        public function setSKU($sku)
        { 
            $this->SKU= $sku;
        }
        public function setName($name)
        { 
            $this->Name=$name;
        }
        public function setPrice($price)
        { 
            $this->Price=$price;
        }
        public function setType($type)
        { 
            $this->Type = $type;
        }

        public function getSKU()
        {
             return $this->SKU;
        }

        public function getName()
        {
             return $this->Name;
        }
        public function getPrice()
        {
             return $this->Price;
        }
        public function getType()
        {
             return $this->Type;
        }

        public function getProperties()
        {
             return $this->Properties;
        }

        public function getTableName()
        {
            return $this->TableName;
        }      
    }

