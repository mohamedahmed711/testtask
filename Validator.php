<?php 

class Validator{
    private $data;
    private $properties = ['SKU'=>'','name'=>'','price'=>'']; 
    private $errors=[];

    public function __construct($form_data)
    {
        $this->data = $form_data;
        $this->validateSKU();
        $this->validateName();
        $this->validatePrice();
        $this->validateType();
    }

    
    
    private function validateSKU()
    {
        $val = trim($this->data['SKU']);
        if (empty($val)) {
            $this->addError('SKU','SKU is required!');
        }else {
            $this->setProperty('SKU',$val);
            if (!preg_match('/^[a-zA-Z]+[0-9]+$/',$val)) {
                $this->addError('SKU','SKU should be in this form: WordNumber');
            }
            if ($this->searchSKUs($val)) {
                $this->addError('SKU','SKU Already Exists');
            }
        }
    }

    private function validateName()
    {
        $val = $this->data['name'];
        if (empty($val)) {
            $this->addError('name','The Name is required!');
        }else {
            $this->setProperty('name',$val);
            if (!preg_match('/^[a-zA-Z]+[0-9]+$/',$val)) {
                $this->addError('name','Name should be in this form: WordNumber');
            }
        }
    }

    private function validatePrice()
    {
        $val = trim($this->data['price']);
        if (empty($val)) {
            $this->addError('price','The Price is required!');
        }else {
            $this->setProperty('price',$val);
            if (!preg_match('/^[0-9]+$/',$val)) {
                $this->addError('price','Price must only be Integers');
            }
        }
    }
    private function validateType()
    {
        $val = $this->data['type'];
        if ($val =='Select Product Type') {
            $this->addError('type','The Type is required!');
        }      
    }
    
    private function searchSKUs($val){
       $SKUs = array();
       $SKUs = array_merge($SKUs, (new Controller('products'))->findAll('SKU', $val));
       $found = false;
        foreach ($SKUs as $SKU) { 
            if ($SKU === $val ) {
                $found = true;
                break;
            }
        }
        return $found;
    }

    private function setProperty($key,$property)
    { 
        $this->properties[$key] = $property;
    }

    private function addError($key,$error)
    { 
        $this->errors[$key] = $error;
    }

    public function getProperties()
    {
        return $this->properties;
    }
    
    public function getErrors()
    { 
        return $this->errors;
    }
}


