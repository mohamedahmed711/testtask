<?php 
require_once('AutoLoader.php');

$dataInputed=['SKU'=>'','name'=>'','price'=>''];

if (isset($_POST['submit'])) {
  $validator = new Validator($_POST); 
  $dataInputed = $validator->getProperties();
  $errors = $validator->getErrors();  
  array_filter($errors) ? : Save($_POST['type'], $_POST);
}

function Save($className, $ProductData)
{
  $product = new $className();
  $product->setinputs($ProductData); 
  $product->save();
        
  header('Location:index.php');
}

  

?>
<!DOCTYPE html>
<html lang="en">
<?php include('templates/header.php') ?>
  <title>ADD Product</title>
  <script src="https://code.jquery.com/jquery-2.2.4.min.js" integrity="sha256-BbhdlvQf/xTY9gja0Dq3HiwQF8LaCRTXxZKRutelT44=" crossorigin="anonymous"></script>
  <script>
    $(document).ready(function() {
      $("#productType").change(function() {
        var type = $("#productType").val();
        $(".details").hide();
        $("."+type).show();
      })
    })
  </script>
</head>

<body>
  <div class="container">
    <div class="row">
          <div class="col-md-12">
          <h1 class="mr-3">Add Product</h1>
          <hr />
          </div>
    </div> 
    <form id="product_form" action="<?php echo $_SERVER['PHP_SELF']?>" method="POST">
      <div class="mb-3">
        <label for="SKU" class="form-label">SKU</label>
        <input type="text" name="SKU" class="form-control" id="sku" value="<?php echo htmlspecialchars($dataInputed['SKU']); ?>">
        <span id="error"><?php echo $errors['SKU'] ?? '' ?></span>
      </div>
      <div class="mb-3">
        <label for="Name" class="form-label">Name</label>
        <input type="text" name="name" class="form-control" id="name" value="<?php echo htmlspecialchars($dataInputed['name']); ?>">
        <span id="error"><?php echo $errors['name'] ?? ''?></span>
      </div>
      <div class="mb-3">
        <label for="Price" class="form-label">Price</label>
        <input type="text" name="price" class="form-control" id="price" value="<?php echo htmlspecialchars($dataInputed['price']); ?>">
        <span id="error"><?php echo $errors['price'] ?? ''?></span>
      </div>
        <div class="mb-3">
            <select class="form-select" id="productType" aria-label="Default select" name="type" >
            <option selected>Select Product Type</option>
            <option value="Book">Book</option>
            <option value="DVD">DVD</option>
            <option value="Furniture">Furniture</option>
            </select>
            <span id="error"><?php echo $errors['type'] ?? ''?></span>
        </div>

      <div id="details-container">

        <div class="DVD details" >
          <div class="mb-3">
            <label for="exampleInputsize" class="form-label">Size (MB)</label>
            <input type="text" name="size" class="form-control" id="size">
          </div>
          <p>Please provide the size in MB</p>
        </div>

        <div class="Book details" >
          <div class="mb-3">
            <label for="exampleInputweight" class="form-label">Weight (KG)</label>
            <input type="text" name="weight" class="form-control" id="weight">
          </div>
          <p>Please provide the weight in KG</p>
        </div>
      </div>

      <div class="Furniture details" >
          <div class="mb-3">
            <label for="exampleInputheight" class="form-label">Height (CM)</label>
            <input type="text" name="height" class="form-control" id="height">
          </div>
          <div class="mb-3">
            <label for="exampleInputwidth" class="form-label">Width (CM)</label>
            <input type="text" name="width" class="form-control" id="width">
          </div>
          <div class="mb-3">
            <label for="length" class="form-label">Length (CM)</label>
            <input type="text" name="length" class="form-control" id="length">
          </div>
          <p>Please provide the dimensions in HxWxL format</p>
        </div>
      <hr />
      <button type="submit" class="btn btn-primary mr-3" name="submit" value="submit">Save</button>
      <input type="button" value="Cancel" class="btn btn-primary mr-3" onClick="document.location.href='index.php'" />
    </form>
  </div>

<?php include('templates/footer.php') ?>    
</html>