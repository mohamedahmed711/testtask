<?php 
  require_once('AutoLoader.php');
  
  $ProductController = new Controller('products');

  $Products = array();
  $products = array_merge($ProductController->getAll('Type','Book'));
  $products = array_merge($products, $ProductController->getAll('Type','DVD'));
  $products = array_merge($products, $ProductController->getAll('Type','Furniture'));

  if (isset($_POST['massDelete']) and isset($_POST['selectedProducts'])) {
    foreach ($_POST['selectedProducts'] as $value) {
      $ProductController->delete('SKU', $value);
    } 
    header('Location:index.php');
  } 
?>


<!DOCTYPE html>
<html lang="en">
  <?php include('templates/header.php') ?>
  <title>Product List</title>
  </head>
  <body>

    <div class="container">
      <nav class="nav justify-content-end">
        <input type="button" value="ADD" class="btn btn-primary" id="add" onClick="document.location.href='addProduct.php'" />
        <form action="<?php echo $_SERVER['PHP_SELF']?>"  method="POST" style="margin:0px; padding=0;">
          <button class="btn btn-dark" id="delete-product-btn" type="submit" name="massDelete" value="Delete">MASS DELETE</button>
      </nav>
      <div class="row">

          <div class="col-md-12">
            <h1 class="mr-3">Product List</h1>
            <hr />
          </div> 

          <table>
          <?php if (!empty($products)) {?>  
            <?php foreach ($products as $product) {?>
                <tr>
                     <div class="card border-dark mb-3">
                        <input class="form-check-input delete-checkbox" type="checkbox"  name="selectedProducts[]" value ="<?php echo htmlspecialchars($product->getSKU()); ?>">
                        <div class="card-body text-center">

                            <h5 class="card-title"><?php echo htmlspecialchars($product->getSKU()) ?></h5>
                            <h6 class="card-subtitle mb-2 text-muted"><?php echo htmlspecialchars($product->getName()) ?></h6>
                            <p class="card-text"><?php echo htmlspecialchars($product->getPrice()).' $' ?></p>

                          <?php switch (htmlspecialchars($product->getType())) { case "DVD":?>
                            <p class="card-text"><?php echo 'Size: '. htmlspecialchars($product->getProperties()).' MB' ?></p>
                          <?php break; case "Book": ?>
                            <p class="card-text"><?php echo 'Weight: '. htmlspecialchars($product->getProperties()).' KG' ?></p>
                          <?php break; case "Furniture":?>
                            <p class="card-text"><?php echo 'Dimensions: '. htmlspecialchars($product->getProperties()) ?></p>
                          <?php break; } ?>

                        </div>
                     </div>
                  </tr>        
          <?php } } ?>
          </table>
        </form>
      </div>
    </div>


    <?php include('templates/footer.php') ?>
    
</html>