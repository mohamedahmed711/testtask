<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/css/bootstrap.min.css" integrity="sha384-KyZXEAg3QhqLMpG8r+8fhAXLRk2vvoC2f3B09zVXn8CA5QIVfZOJ3BCsw2P0p/We" crossorigin="anonymous">
    <style>
            .card{ margin: 15px; padding: 10px; width: 16rem; }

            .DVD,.Book,.Furniture{ display:none;}
            
            #add{ height: 40px; width:fit-content; margin:15pt; }

            #error{ color:red;}
            
            footer { color: #707070; height: 2em; width:100%; }

            form{ max-width: 460px; margin: 20px auto; padding: 20px;}

            nav,div.col{ margin: 20px auto; padding: 20px;}
    </style>

    
